#pragma once

class ClssSrf02 {

private:
    int reading,
        sda,
        scl,
        addressI2C; //=112 en décimal
    
public:
    ClssSrf02(int prmAddressi2C, int prmSda, int prmScl);
    ~ClssSrf02();
    int LireDistance();
};