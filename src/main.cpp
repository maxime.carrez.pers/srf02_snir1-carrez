#include <Arduino.h>
#include "ClssSrf02.hpp"
ClssSrf02 Capteur(0x70, D2, D1);

int distance = 0;

void setup()
{
Serial.begin(9600); // start serial communication at 9600bps
}

void loop()
{
distance = Capteur.LireDistance();
Serial.print(distance); // print the reading
Serial.println("cm");
}